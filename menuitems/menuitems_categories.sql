CREATE TABLE menuitems_categories
(
    category_id varchar(50),
    id varchar(24),
    index	integer,
    name varchar(50),
    origin_id varchar(50),
    origin_name varchar(50),
    parent_index varchar(50)
);

ALTER TABLE public.menuitems_categories
   ADD CONSTRAINT menuitems_categories_pkey
   PRIMARY KEY (id, index, parent_index);
