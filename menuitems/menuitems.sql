CREATE TABLE menuitems
(
    cost float8,
    cost_center_id varchar(50),
    cost_center_name varchar(50),
    cost_center_origin_id varchar(50),
    cost_center_origin_name varchar(100),
    created timestamp,
    creator varchar(50),
    date varchar(50),
    description varchar(200),
    id varchar(24) NOT NULL,
    index integer,
    is_archived boolean,
    location_id varchar(50),
    location_name varchar(50),
    location_subsidiary varchar(50),
    name varchar(200),
    origin_id varchar(50),
    parent_index varchar(50),
    source_system varchar(50),
    unit_price float8,
    updated timestamp,
    updater varchar(50)
);

ALTER TABLE public.menuitems
   ADD CONSTRAINT menuitems_pkey
   PRIMARY KEY (id);

ALTER TABLE public.menuitems
   ADD CONSTRAINT menuitems_unique
   UNIQUE (origin_id, date, cost_center_origin_id);
