CREATE TABLE itemsum
(
    cogs float8,
    cost_center_id varchar(50),
    cost_center_name varchar(50),
    cost_center_origin_id varchar(50),
    cost_center_origin_name varchar(50),
    created timestamp,
    creator varchar(50),
    date timestamp,
    gross_amt float8,
    id varchar(24),
    index integer,
    is_archived boolean,
    location_id varchar(50),
    location_name varchar(50),
    location_subsidiary varchar(50),
    name varchar(100),
    net_amt float8,
    origin_id varchar(50),
    origin_location_id varchar(50),
    parent_index varchar(50),
    plu varchar(50),
    qty integer,
    source_system varchar(50),
    spoilage_amt float8,
    spoilage_qty integer,
    type varchar(50),
    updated timestamp,
    updater varchar(50)
);

ALTER TABLE public.itemsum
   ADD CONSTRAINT itemsum_pkey
   PRIMARY KEY (id);

ALTER TABLE public.itemsum
   ADD CONSTRAINT itemsum_unique
   UNIQUE (location_id, source_system, origin_id, date, cost_center_origin_id);
