CREATE TABLE events
(
    access_start timestamp,
    created timestamp,
    creator varchar(50),
    description varchar(50),
    end_time timestamp,
    id varchar(24),
    index integer,
    is_archived boolean,
    location_id varchar(50),
    location_name varchar(50),
    location_subsidiary varchar(50),
    name varchar(100),
    origin_id varchar(50),
    origin_location_id varchar(50),
    parent_index varchar(50),
    source_system varchar(50),
    start_time timestamp,
    type varchar(50),
    updated timestamp,
    updater varchar(50)
);

ALTER TABLE public.events
   ADD CONSTRAINT events_pkey
   PRIMARY KEY (id);

ALTER TABLE public.events
   ADD CONSTRAINT events_unique
   UNIQUE (location_id, source_system, origin_id);
