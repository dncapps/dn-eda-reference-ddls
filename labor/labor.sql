CREATE TABLE labor
(
    adjusted_apply_date timestamp,
    apply_date timestamp,
    cost_center_id varchar(50),
    cost_center_name varchar(50),
    cost_center_origin_id varchar(50),
    cost_center_origin_name varchar(50),
    created timestamp,
    creator varchar(50),
    emplid varchar(50),
    enddtm timestamp,
    extract_time timestamp,
    firstnm varchar(50),
    id varchar(24),
    index integer,
    is_archived boolean,
    is_deleted boolean,
    job_code varchar(50),
    job_name varchar(50),
    job_type varchar(50),
    laboracctname varchar(100),
    lastnm varchar(50),
    location_id varchar(50),
    location_name varchar(50),
    location_subsidiary varchar(50),
    middleinitialnm varchar(50),
    money_amount float8,
    origin_id varchar(50),
    origin_location_id varchar(50),
    parent_index varchar(50),
    paycode_name varchar(50),
    paycode_type varchar(50),
    source_system varchar(50),
    stand_code varchar(50),
    startdtm timestamp,
    time_in_seconds integer,
    updated timestamp,
    updatedtm timestamp,
    updater varchar(50),
    wage_amount float8
);

ALTER TABLE public.labor
   ADD CONSTRAINT labor_pkey
   PRIMARY KEY (id);

ALTER TABLE public.labor
   ADD CONSTRAINT labor_unique
   UNIQUE (location_id, source_system, origin_id);