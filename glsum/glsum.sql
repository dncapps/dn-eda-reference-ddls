CREATE TABLE glsum
(
    account_description varchar(50),
    amount float8,
    center_description varchar(50),
    cost_center_id varchar(50),
    cost_center_name varchar(50),
    cost_center_origin_id varchar(50),
    cost_center_origin_name varchar(50),
    created timestamp,
    creator varchar(50),
    credit float8,
    date timestamp,
    debit float8,
    department_id varchar(50),
    extract_time timestamp,
    gl_code varchar(50),
    id varchar(24),
    index integer,
    is_archived boolean,
    location_id varchar(50),
    location_name varchar(50),
    location_subsidiary varchar(50),
    parent_index varchar(50),
    product_code varchar(50),
    segment1 varchar(50),
    source_system varchar(50),
    updated timestamp,
    updater varchar(50)
);

ALTER TABLE public.glsum
   ADD CONSTRAINT glsum_pkey
   PRIMARY KEY (id);

ALTER TABLE public.glsum
   ADD CONSTRAINT glsum_unique
   UNIQUE (date, cost_center_origin_id, product_code, gl_code, debit, credit);
