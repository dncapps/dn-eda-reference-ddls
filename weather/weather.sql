CREATE TABLE weather
(
    apparent_temperature float8,
    cloud_cover float8,
    created timestamp,
    creator varchar(50),
    datetime timestamp,
    dew_point float8,
    humidity float8,
    icon varchar(50),
    id varchar(24) NOT NULL,
    index integer,
    is_archived boolean,
    location_id varchar(50),
    ozone float8,
    parent_index varchar(50),
    precipitation_accumulation float8,
    precipitation_intensity float8,
    precipitation_probability float8,
    precipitation_type varchar(50),
    pressure float8,
    source_system varchar(50),
    summary varchar(50),
    temperature float8,
    type varchar(50),
    updated timestamp,
    updater varchar(50),
    uv_index integer,
    visibility float8,
    wind_bearing integer,
    wind_gust float8,
    wind_speed float8
);

ALTER TABLE public.weather
   ADD CONSTRAINT weather_pkey
   PRIMARY KEY (id);

ALTER TABLE public.weather
   ADD CONSTRAINT weather_unique
   UNIQUE (location_id, source_system, datetime);
