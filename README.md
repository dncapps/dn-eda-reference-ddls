# Reference EDA DDLs
This repository contains reference DDLs to store data in relational databases.

## Transactions
Due to the nested nature of our transactions data, we have multiple tables that
are used in conjunction to completely describe a transaction as it occurs at a
POS.

| Table Name                                     | Description                                                                     |
|------------------------------------------------|---------------------------------------------------------------------------------|
| `transactions`                                 | High-level transaction data such as cost center, totals, tax, and employee.     |
| `transactions_customer_addresses`              | Customer address information.
|
| `transactions_customer_phone_numbers`          | Customer phone number information.
|
| `transactions_fee_items`                       | Fees related to a transaction, such as administrative charges.                  |
| `transactions_loyalty`                         | Loyalty information, such as a barcode.
|
| `transactions_price_modifiers`                 | Items that modify the price of an item or transaction, such as a discount.      |
| `transactions_references`                      | References, such as a transfer
|
| `transactions_sale_items`                      | Data about items that were sold in a transaction.                               |
| `transactions_sale_items_categories`           | The categories that sale items belong to.                                       |
| `transactions_sale_items_modifiers`            | Modifiers that are applied to specific sale items, such as condiments.          |
| `transactions_sale_items_modifiers_categories` | The categories that sale items modifiers belong to.                             |
| `transactions_tax_items`                       | Tax items that are applied to transactions.                                     |
| `transactions_tender_items`                    | Tenders supplied during a transaction, including credit card, cash, and change. |