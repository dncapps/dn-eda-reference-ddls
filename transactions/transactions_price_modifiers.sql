CREATE TABLE transactions_price_modifiers
(
   amount                     float8,
   code                       varchar(50),
   date_posted                timestamp,
   id                         varchar(24)    NOT NULL,
   index                      integer        NOT NULL,
   name                       varchar(150),
   origin_employee_first      varchar(50),
   origin_employee_id         varchar(50),
   origin_employee_last       varchar(50),
   parent_index               integer        NOT NULL,
   sale_item_modifier_ref_id  varchar(50),
   sale_item_origin_id        varchar(50),
   status                     varchar(50),
   third_party_id             varchar(50),
);

ALTER TABLE public.transactions_price_modifiers
   ADD CONSTRAINT transactions_price_modifiers_pkey
   PRIMARY KEY (id, index, parent_index);
