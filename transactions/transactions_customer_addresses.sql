CREATE TABLE transactions_customer_addresses
(
    address1                                 varchar(128),
    address2                                 varchar(128),
    city                                     varchar(50),
    id                                       varchar(24)   NOT NULL,
    index                                    integer,
    parent_index                             integer,
    state                                    varchar(50),
    type                                     varchar(50),
    zip                                      varchar(50)
);

ALTER TABLE public.transactions_customer_addresses
   ADD CONSTRAINT transactions_customer_addresses_pkey
   PRIMARY KEY (id, index, parent_index);
