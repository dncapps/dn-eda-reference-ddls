CREATE TABLE transactions_tender_items
(
    amount                 float8,
    authorization_id       varchar(100),
    charge_tip             float8,
    date_posted            timestamp,
    gateway_comment        varchar(200),
    id                     varchar(24)   NOT NULL,
    index                  integer       NOT NULL,
    is_card_present        varchar(10),
    last4                  varchar(4),
    origin_employee_first  varchar(50),
    origin_employee_id     varchar(50),
    origin_employee_last   varchar(50),
    parent_index           integer       NOT NULL,
    payment_status         varchar(50),
    third_party_id         varchar(200),
    type                   varchar(50)
);

ALTER TABLE public.transactions_tender_items
   ADD CONSTRAINT transactions_tender_items_pkey
   PRIMARY KEY (id, index, parent_index);
