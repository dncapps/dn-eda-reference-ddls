CREATE TABLE transactions_references
(
   direction        varchar(50),
   id               varchar(24)   NOT NULL,
   index            integer       NOT NULL,
   origin_id        varchar(50),
   parent_index     integer       NOT NULL,
   type             varchar(50),
);

ALTER TABLE public.transactions_references
   ADD CONSTRAINT transactions_references_pkey
   PRIMARY KEY (id, index, parent_index);
