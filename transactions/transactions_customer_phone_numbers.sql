CREATE TABLE transactions_customer_phone_numbers
(
   id                                       varchar(24)   NOT NULL,
   index                                    integer,
   parent_index                             integer,
   phone_number                             varchar(50),
   type                                     varchar(50)
);

ALTER TABLE public.transactions_customer_phone_numbers
   ADD CONSTRAINT transactions_customer_phone_numbers_pkey
   PRIMARY KEY (id, index, parent_index);
