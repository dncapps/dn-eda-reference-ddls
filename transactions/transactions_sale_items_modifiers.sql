CREATE TABLE transactions_sale_items_modifiers
(
    amount          float8,
    id              varchar(24)   NOT NULL,
    index           integer       NOT NULL,
    line_id         varchar(50),
    name            varchar(75),
    origin_id       varchar(50),
    parent_index    integer       NOT NULL,
    quantity        float8,
    ref_id          varchar(50),
    status          varchar(50),
    third_party_id  varchar(50)
);

ALTER TABLE public.transactions_sale_items_modifiers
   ADD CONSTRAINT transactions_sale_items_modifiers_pkey
   PRIMARY KEY (id, index, parent_index);
