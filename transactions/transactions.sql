CREATE TABLE transactions
(
   comment                                  varchar(200),
   cost_center_id                           varchar(50),
   cost_center_name                         varchar(50),
   cost_center_origin_id                    varchar(50),
   cost_center_origin_name                  varchar(100),
   cost_center_origin_terminal_description  varchar(200),
   cost_center_origin_terminal_id           varchar(50),
   cost_center_table_number                 varchar(50),
   covers                                   integer,
   created                                  timestamp,
   creator                                  varchar(50),
   customer_email_address                   varchar(128),
   customer_fax_number                      varchar(50),
   customer_first_name                      varchar(200),
   customer_is_deleted                      boolean,
   customer_last_name                       varchar(200),
   customer_member_type                     varchar(50),
   customer_name                            varchar(100),
   customer_origin_id                       varchar(200),
   date_closed                              timestamp,
   date_opened                              timestamp,
   date_posted                              timestamp,
   destination                              varchar(100),
   event_id                                 varchar(50),
   event_origin_id                          varchar(50),
   event_origin_name                        varchar(50),
   extract_time                             timestamp,
   id                                       varchar(24)    NOT NULL,
   index                                    integer,
   is_archived                              varchar(10),
   location_id                              varchar(10),
   location_name                            varchar(50),
   location_subsidiary                      varchar(10),
   number                                   integer,
   order_status                             varchar(50),
   origin_employee_first                    varchar(50),
   origin_employee_id                       varchar(50),
   origin_employee_last                     varchar(100),
   origin_id                                varchar(50),
   origin_id_int                            integer,
   parent_index                             integer,
   pos_type                                 varchar(50),
   pos_location_business_id                 varchar(50),
   pos_location_department_id               varchar(50),
   pos_location_operating_id                varchar(50),
   shipping_amount                          float8,
   source_system                            varchar(50),
   tax_amount                               float8,
   third_party_id                           varchar(50),
   total                                    float8,
   updated                                  timestamp,
   updater                                  varchar(50)
);

ALTER TABLE public.transactions
   ADD CONSTRAINT transactions_pkey
   PRIMARY KEY (id);

ALTER TABLE public.transactions
   ADD CONSTRAINT transactions_unique
   UNIQUE (location_id, source_system, origin_id);
