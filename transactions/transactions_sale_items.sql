CREATE TABLE transactions_sale_items
(
    amount            float8,
    comment           varchar(128),
    cost              float8,
    date_posted       timestamp,
    description       varchar(200),
    id                varchar(24)   NOT NULL,
    index             integer       NOT NULL,
    is_tax_exempt     boolean,
    name              varchar(200),
    origin_employee_first  varchar(50),
    origin_employee_id     varchar(50),
    origin_employee_last   varchar(50),
    origin_id         varchar(50),
    parent_index      integer       NOT NULL,
    quantity          integer,
    status            varchar(50),
    third_party_id    varchar(50),
    unit_of_measure   varchar(50),
    units             float8,
    vendor_code       varchar(50)
);

ALTER TABLE public.transactions_sale_items
   ADD CONSTRAINT transactions_sale_items_pkey
   PRIMARY KEY (id, index, parent_index);
