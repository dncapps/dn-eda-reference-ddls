CREATE TABLE transactions_sale_items_categories
(
    id            varchar(24)   NOT NULL,
    index         integer       NOT NULL,
    name          varchar(50),
    origin_id     varchar(50),
    parent_index  integer       NOT NULL,
    type          varchar(50)
);

ALTER TABLE public.transactions_sale_items_categories
   ADD CONSTRAINT transactions_sale_items_categories_pkey
   PRIMARY KEY (id, index, parent_index);
