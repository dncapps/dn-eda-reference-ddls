CREATE TABLE transactions_fee_items
(
    amount        float8,
    id            varchar(24)   NOT NULL,
    index         integer       NOT NULL,
    origin_id     varchar(50),
    parent_index  integer       NOT NULL,
    sale_item_origin_id varchar(50),
    type          varchar(100)
);

ALTER TABLE public.transactions_fee_items
   ADD CONSTRAINT transactions_fee_items_pkey
   PRIMARY KEY (id, index, parent_index);