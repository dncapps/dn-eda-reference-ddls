CREATE TABLE transactions_tax_items
(
    amount                float8,
    date_posted           timestamp,
    id                    varchar(24)   NOT NULL,
    index                 integer       NOT NULL,
    is_inclusive          boolean,
    name                  varchar(50),
    parent_index          integer       NOT NULL,
    sale_item_origin_id   varchar(50)
);

ALTER TABLE public.transactions_tax_items
   ADD CONSTRAINT transactions_tax_items_pkey
   PRIMARY KEY (id, index, parent_index);
