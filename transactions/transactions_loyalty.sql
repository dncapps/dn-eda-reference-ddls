CREATE TABLE transactions_loyalty
(
   barcode          varchar(50)
   id               varchar(24)   NOT NULL,
   index            integer       NOT NULL,
   origin_id        varchar(50),
   parent_index     integer       NOT NULL,
   type             varchar(50),
);

ALTER TABLE public.transactions_loyalty_pkey
   ADD CONSTRAINT transactions_loyalty_pkey
   PRIMARY KEY (id, index, parent_index);
