CREATE TABLE attendance
(
    cost_center_id varchar(50),
    cost_center_name varchar(50),
    cost_center_origin_id varchar(50),
    cost_center_origin_name varchar(50),
    created timestamp,
    creator varchar(50),
    date timestamp,
    extract_time timestamp,
    id varchar(24),
    index integer,
    is_archived boolean,
    location_id varchar(50),
    location_name varchar(50),
    location_subsidiary varchar(50),
    parent_index varchar(50),
    source_system varchar(50),
    total integer,
    updated timestamp,
    updater varchar(50)
);

ALTER TABLE public.attendance
   ADD CONSTRAINT attendance_pkey
   PRIMARY KEY (id);

ALTER TABLE public.attendance
   ADD CONSTRAINT attendance_unique
   UNIQUE (date, cost_center_origin_id);
